//
//  DetailViewController.h
//  BannersOnAList
//
//  Created by Zoltan Ulrich on 25.11.2013.
//  Copyright (c) 2013 iQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
