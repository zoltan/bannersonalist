//
//  MasterViewController.m
//  BannersOnAList
//
//  Created by Zoltan Ulrich on 25.11.2013.
//  Copyright (c) 2013 iQuest Technologies. All rights reserved.
//

#import "MasterViewController.h"

#import "DetailViewController.h"

@interface MasterViewController ()<ATBannerViewDelegate>
{
    NSMutableArray *_objects;
    NSArray *_aliases;
}
@end

@implementation MasterViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _aliases = @[@"listright-top-5",
                 @"listleft-top-5",
                 @"listright-midt-5",
                 @"listleft-midt-5",
                 @"listleft-bottom-5",
                 @"listright-bottom-5"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;

    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender
{
    if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
    }
    [_objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    ATBannerView *existingBannerView = (ATBannerView*)[cell viewWithTag:1000];
    
    if (indexPath.row % 5 == 0)
    {
        if (nil == existingBannerView)
        {
            ATBannerView *bannerView = [[ATBannerView alloc] initWithFrame:cell.bounds];
            bannerView.delegate = self;
            bannerView.viewController = self;
            
            ATAdtechAdConfiguration *configuration = [ATAdtechAdConfiguration configuration];
            configuration.networkID = 263;
            configuration.subNetworkID = 0;
            configuration.alias = _aliases[arc4random_uniform(_aliases.count)];
            configuration.cacheableMediaExtensions = @[@"mp4", @"webm"];
            bannerView.configuration = configuration;
            bannerView.tag = 1000;
            
            [cell.contentView addSubview:bannerView];
            
            [bannerView load];
//
//            double delayInSeconds = 5.0;
//            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//                [bannerView removeFromSuperview];
//            });
        }
        else
        {
            [existingBannerView load];
        }
    }
    else
    {
        if (nil != existingBannerView)
        {
            [existingBannerView removeFromSuperview];
        }
        
        NSDate *object = _objects[indexPath.row];
        cell.textLabel.text = [object description];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row % 5 == 0)
    {
        return 250;
    }
    else
    {
        return tableView.rowHeight;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath NS_AVAILABLE_IOS(6_0)
{
    ATBannerView *bannerView = (ATBannerView*)[cell viewWithTag:1000];
    [bannerView removeFromSuperview];
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSDate *object = _objects[indexPath.row];
        [[segue destinationViewController] setDetailItem:object];
    }
}

#pragma mark - ATBannerViewDelegate

- (void)didFetchNextAd:(ATBannerView*)view
{
}

- (void)didFailFetchingAd:(ATBannerView*)view
{
}

@end
