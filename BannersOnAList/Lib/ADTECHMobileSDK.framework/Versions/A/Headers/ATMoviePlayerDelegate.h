//
//  ATMoviePlayerDelegate.h
//  ADTECHMobileSDK
//
//  Created by ADTECH GmbH on 10/24/13.
//  Copyright (c) 2013 ADTECH GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ATBrowserViewController;

/**
 * Implement this protocol to be notified of different events in the lifecycle of the movie player.
 *
 * @since 3.4
 */
@protocol ATMoviePlayerDelegate <NSObject>
@optional

/**
 * Called when a video ad tries to show a landing page, giving you the chance to allow or deny it.
 * You can also provide your own custom implementation for the In-App browser used.
 *
 * @since 3.4
 *
 * @param URL The URL that needs to be loaded
 * @param browserViewController Your custom browser view controller that needs to inherit ATBrowserViewController class
 *
 * @see ATBrowserViewController
 *
 * @discussion If you don't set the 'browserViewController' parameter, the SDK will use its own in-app browser for WEB URLs.
 *             If the URL type is Native or InApp (like an AppStore URL that will show SKStoreProductViewController on iOS 6.0 or greater), 
 *             the SDK will use its own mechanism to open it.
 *             You can provide your own in-app browser like this:
 *
 * <pre><code>
 *   - (BOOL)shouldOpenLandingPageWithURL:(NSURL*)URL useBrowser:(ATBrowserViewController**)browserViewController
 *   {
 *      &emsp;&emsp;MyCustomInAppBrowser *browser = [[ MyCustomInAppBrowser alloc] initWithURL:URL nibName:nil bundle:nil];
 *      &emsp;&emsp;*browserViewController = browser;
 *
 *      &emsp;&emsp;return YES;
 *   }
 * </code></pre>
 */
- (BOOL)shouldOpenLandingPageWithURL:(NSURL*)URL useBrowser:(ATBrowserViewController**)browserViewController;

/**
 * Called when the player is about to start playback of a linear ad.
 *
 * @param type The linear ad type
 *
 * @since 3.4
 */
- (void)playerWillStartPlayingLinearAdOfType:(ATVideoAdType)type;

/**
 * Called when the player ends the playback of a linear ad. This can happen when the linear naturally reaches the end of its content,
 * when it is skipped, when the landing page is shown and dismissed (the remaining part of the linear is skipped) or when the player is stopped.
 *
 * @param type The linear ad type
 *
 * @since 3.4
 */
- (void)playerDidStopPlayingLinearAdOfType:(ATVideoAdType)type;

@end
