//
//  ATVASTErrorCodes.h
//  ADTECHMobileSDK
//
//  Created by ADTECH GmbH on 04.11.2013.
//  Copyright (c) 2013 ADTECH GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kATVASTParserErrorDomain;
extern NSString *const kATVASTRequestErrorDomain;
extern NSString *const kATVASTLinearErrorDomain;

extern const NSUInteger kATVASTParserErrorCodeCanceled;
extern const NSUInteger kATVASTParserErrorCodeMalformed;

extern const NSUInteger kATVASTErrorCodeNoLinearMedia;
extern const NSUInteger kATVASTErrorCodeNoMediaFilesInLinear;
extern const NSUInteger kATVASTErrorCodeFailedLoadingTracks;
extern const NSUInteger kATVASTErrorCodeFailedPreparingVideoItem;
extern const NSUInteger kATVASTErrorCodeInvalidConfiguration;
extern const NSUInteger kATVASTErrorCodeNoNonLinear;
extern const NSUInteger kATVASTErrorCodeTooManyWrapperRedirects;
