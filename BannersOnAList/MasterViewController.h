//
//  MasterViewController.h
//  BannersOnAList
//
//  Created by Zoltan Ulrich on 25.11.2013.
//  Copyright (c) 2013 iQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MasterViewController : UITableViewController

@end
